import VisualizationComponent from './components';
import Dummy from './components/Dummy';
import LineChart from './components/VCharts';
import ParallelCoordinates from './components/ParallelCoordinates';

var components = {
  Dummy,
  LineChart,
  ParallelCoordinates
};

export {
  VisualizationComponent,
  components
};
